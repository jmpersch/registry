# Maven Package Registry

This repository contains the published artifacts of my software projects.

The projects are licensed under the GNU Affero General Public License, Version 3, or the Apache License, Version 2.0, respectively.

[→ Search Package Registry](https://gitlab.com/jmpersch/registry/-/packages)

## Use this repository in your software projects

### Maven

```xml
<repositories>
    <!--
        The Maven package registry of my software projects, hosted on GitLab:
        https://gitlab.com/jmpersch/registry
    -->
    <repository>
        <url>https://gitlab.com/api/v4/projects/41417498/packages/maven</url>
    </repository>
</repositories>
```

### Gradle

#### Groovy

```gradle
repositories {
	
    // The Maven package registry of my software projects, hosted on GitLab:
    // <https://gitlab.com/jmpersch/registry>
    maven { url "https://gitlab.com/api/v4/projects/41417498/packages/maven" }
}
```

#### Kotlin

```kotlin
repositories {
	
    // The Maven package registry of my software projects, hosted on GitLab:
    // <https://gitlab.com/jmpersch/registry>
    maven("https://gitlab.com/api/v4/projects/41417498/packages/maven")
}
```

- - - -

Copyright © Jan Martin Persch. All rights reserved.